# CIDR computation in TypeScript
Currently only works with IPv4 address space. Enables parsing strings and formatting to a string. Additionally provides
functions to split a CIDRv4 up into multiple CIDRv4 parts. For example:
```typescript
const a = new CIDRv4(new IPv4(0, 0, 0, 0), 0);
const b = CIDRv4.parseString('0.0.0.0/0');
const c = a.splitInEqualSubCIDRs(4); // 0.0.0.0/2, 64.0.0.0/2, 128.0.0.0/2, 192.0.0.0/2
const d = a.splitInSubCIDRs({ newBits: 2, name: 'private' }, { newBits: 2, name: 'public' });
const e = d.get('private'); // 0.0.0.0/2
const f = a.splitInSubCIDRsBySize({ size: 256, name: 'private' }, { size: 24, name: 'public' });
const g = f.get('public'); // 0.0.1.0/27
```

import { expect } from 'chai';
import { CIDRv4 } from '../src/cidrv4';
import { IPv4 } from '../src/ipv4';

describe('CIDRv4', () => {
  it('Accepts valid IPv4 and mask', () => {
    new CIDRv4(new IPv4(0, 0, 0, 0), 0);
    new CIDRv4(new IPv4(0, 0, 0, 0), 32);
  });

  it('Rejects invalid IPv4 or mask', () => {
    // @ts-ignore
    expect(() => new CIDRv4(null, 0)).to.throw('Invalid IPv4 for CIDRv4.');
    expect(() => new CIDRv4(new IPv4(0, 0, 0, 0), -1)).to.throw('Invalid mask for CIDRv4.');
    expect(() => new CIDRv4(new IPv4(0, 0, 0, 0), 33)).to.throw('Invalid mask for CIDRv4.');
  });

  // First IP, last IP, total IP addresses, including staring with IP in the middle
  it('Computes CIDR range correctly', () => {
    for (let value of [
      {
        cidr: new CIDRv4(new IPv4(255, 255, 255, 255), 8),
        octetsFirst: [255, 0, 0, 0],
        octetsLast: [255, 255, 255, 255],
        totalIPs: 16777216
      },
      {
        cidr: new CIDRv4(new IPv4(255, 255, 255, 255), 24),
        octetsFirst: [255, 255, 255, 0],
        octetsLast: [255, 255, 255, 255],
        totalIPs: 256
      },
      {
        cidr: new CIDRv4(new IPv4(128, 128, 128, 128), 8),
        octetsFirst: [128, 0, 0, 0],
        octetsLast: [128, 255, 255, 255],
        totalIPs: 16777216
      },
      {
        cidr: new CIDRv4(new IPv4(128, 128, 128, 128), 24),
        octetsFirst: [128, 128, 128, 0],
        octetsLast: [128, 128, 128, 255],
        totalIPs: 256
      }
    ]) {
      const octetsFirst = value.cidr.FirstIP.asOctets();
      const octetsLast = value.cidr.LastIP.asOctets();
      expect(octetsFirst).to.have.ordered.members(value.octetsFirst);
      expect(octetsLast).to.have.ordered.members(value.octetsLast);
      expect(value.cidr.TotalIPs).to.equal(value.totalIPs);
    }
  });

  it('Splits in equal sub CIDR ranges correctly', () => {
    for (let cidr of [
      new CIDRv4(new IPv4(0, 0, 0, 0), 24),
      new CIDRv4(new IPv4(0, 0, 0, 128), 24)
    ]) {
      for (let splitValue of [
        { count: 2, cidrs: ['0.0.0.0/25', '0.0.0.128/25'] },
        { count: 3, cidrs: ['0.0.0.0/26', '0.0.0.64/26', '0.0.0.128/26'] },
        { count: 4, cidrs: ['0.0.0.0/26', '0.0.0.64/26', '0.0.0.128/26', '0.0.0.192/26'] },
        { count: 5, cidrs: ['0.0.0.0/27', '0.0.0.32/27', '0.0.0.64/27', '0.0.0.96/27', '0.0.0.128/27'] }]) {
        const subCIDRs = cidr.splitInEqualSubCIDRs(splitValue.count);
        const strings = subCIDRs.map(x => x.toString());
        expect(subCIDRs.length).to.equal(splitValue.count);
        expect(strings).to.have.ordered.members(splitValue.cidrs);
      }
    }
  });

  it('Throws an error when splitting equally is not possible', () => {
    const cidrv4 = new CIDRv4(new IPv4(0, 0, 0, 0), 24);

    expect(() => cidrv4.splitInEqualSubCIDRs(1)).to.throw('Must be split in at least 2.');
    expect(() => cidrv4.splitInEqualSubCIDRs(257)).to.throw('Can\'t split in more than the total IPs available.');
  });

  describe('Splits in segments correctly', () => {
    it('Handles start correctly', () => {
      const cidr = new CIDRv4(new IPv4(0, 0, 0, 0), 0);
      const subCIDRs = cidr.splitInSubCIDRs({ newBits: 1, name: '1' }, { newBits: 2, name: '2' },
        { newBits: 3, name: '3' }, { newBits: 5, name: '5' });

      expect(subCIDRs.size).to.equal(4);
      expect(subCIDRs.get('1')!.toString()).to.equal('0.0.0.0/1');
      expect(subCIDRs.get('2')!.toString()).to.equal('128.0.0.0/2');
      expect(subCIDRs.get('3')!.toString()).to.equal('192.0.0.0/3');
      expect(subCIDRs.get('5')!.toString()).to.equal('224.0.0.0/5');
    });

    it('Handles with mask correctly', () => {
      const cidr = new CIDRv4(new IPv4(0, 0, 0, 0), 24);
      const subCIDRs = cidr.splitInSubCIDRs({ newBits: 1, name: '1' }, { newBits: 2, name: '2' }, {
        newBits: 3,
        name: '3'
      }, { newBits: 5, name: '5' });

      expect(subCIDRs.size).to.equal(4);
      expect(subCIDRs.get('1')!.toString()).to.equal('0.0.0.0/25');
      expect(subCIDRs.get('2')!.toString()).to.equal('0.0.0.128/26');
      expect(subCIDRs.get('3')!.toString()).to.equal('0.0.0.192/27');
      expect(subCIDRs.get('5')!.toString()).to.equal('0.0.0.224/29');
    });
  });

  it('Throws an error when splitting in segments is not possible', () => {
    const cidrv4 = new CIDRv4(new IPv4(0, 0, 0, 0), 0);

    for (let segments of [
      [{ newBits: -1, name: '-1' }],
      [{ newBits: 1, name: '1' }, { newBits: -1, name: '-1' }]
    ]) {
      expect(() => cidrv4.splitInSubCIDRs(...segments)).to.throw('All segments must have at least one bit.');
    }

    for (let segments of [
      [{ newBits: 1, name: '' }],
      [{ newBits: 1, name: '1' }, { newBits: 1, name: '' }],
      [{ newBits: 1, name: null }, { newBits: 1, name: '' }],
      [{ newBits: 1, name: '1' }, { newBits: 1, name: null }]
    ]) {
      // @ts-ignore
      expect(() => cidrv4.splitInSubCIDRs(...segments)).to.throw('All segments must have a name.');
    }

    for (let segments of [
      [{ newBits: 33, name: '33' }],
      [{ newBits: 1, name: '1' }, { newBits: 33, name: '33' }]
    ]) {
      expect(() => cidrv4.splitInSubCIDRs(...segments)).to.throw('32 bits available, but a segment with more was given.');
    }

    for (let segments of [
      [{ newBits: 1, name: '1' }, { newBits: 1, name: '1' }],
      [{ newBits: 2, name: '2a' }, { newBits: 2, name: '2b' }, { newBits: 2, name: '2a' }]
    ]) {
      expect(() => cidrv4.splitInSubCIDRs(...segments)).to.throw('All segments must have an unique name.');
    }

    for (let segments of [
      [{ newBits: 1, name: '1a' }, { newBits: 1, name: '1b' }, { newBits: 1, name: '1c' }],
      [{ newBits: 2, name: '2a' }, { newBits: 1, name: '1' }, { newBits: 2, name: '2b' }, { newBits: 2, name: '2c' }]
    ]) {
      expect(() => cidrv4.splitInSubCIDRs(...segments)).to.throw('Too many IPv4 addresses requested.');
    }
  });

  it('Splits in segments by size correctly', () => {
    const cidrv4 = new CIDRv4(new IPv4(0, 0, 0, 0), 0);
    const subCIDRs = cidrv4.splitInSubCIDRsBySize({ size: 8, name: '8' }, { size: 9, name: '9' }, { size: 511, name: '511' });

    expect(subCIDRs.size).to.equal(3);
    expect(subCIDRs.get('511')!.toString()).to.equal('0.0.0.0/23');
    expect(subCIDRs.get('9')!.toString()).to.equal('0.0.2.0/28');
    expect(subCIDRs.get('8')!.toString()).to.equal('0.0.2.16/29');
  });

  it('Converts to string properly', () => {
    for (let value of [
      { ip: new IPv4(0, 0, 0, 0), mask: 0, expected: '0.0.0.0/0' },
      { ip: new IPv4(0, 0, 0, 0), mask: 32, expected: '0.0.0.0/32' },
      { ip: new IPv4(1, 1, 1, 1), mask: 0, expected: '1.1.1.1/0' },
      { ip: new IPv4(255, 255, 255, 255), mask: 8, expected: '255.255.255.255/8' }
    ]) {
      const cidrv4 = new CIDRv4(value.ip, value.mask);
      const string = cidrv4.toString();
      expect(string).to.equal(value.expected);
    }
  });
  it('Accepts valid string for parsing', () => {
    for (let value of [
      { string: '0.0.0.0/0', octets: [0, 0, 0, 0], mask: 0 },
      { string: '0.0.0.0/32', octets: [0, 0, 0, 0], mask: 32 },
      { string: '1.1.1.1/0', octets: [1, 1, 1, 1], mask: 0 },
      { string: '255.255.255.255/8', octets: [255, 255, 255, 255], mask: 8 }
    ]) {
      const cidrv4 = CIDRv4.parseString(value.string);
      const octets = cidrv4.IP.asOctets();
      expect(octets).to.have.ordered.members(value.octets);
      expect(cidrv4.Mask).to.equal(value.mask);
    }
  });

  it('Rejects invalid string for parsing', () => {
    for (let value of ['', 'a.b.c.d', 'a.b.c.d/e', 'a.b.c.d/0',
      ' 0.0.0.0/0', '0.0.0.0/0 ', '0.0.0.0/ 0', '0.0.0.0 /0',
      '0.0.0.0/a000', '0.0.0.0/-1', '0.0.0.0/a']) {
      expect(() => CIDRv4.parseString(value)).to.throw('Invalid format for CIDRv4.');
    }

    expect(() => CIDRv4.parseString('0.0.0.0/33')).to.throw('Invalid mask for CIDRv4.');

    for (let value of ['999.0.0.0/0', '0.999.0.0/0', '0.0.999.0/0', '0.0.0.999/0']) {
      expect(() => CIDRv4.parseString(value)).to.throw('Invalid octets for IPv4.');
    }
  });
});

import { expect } from 'chai';
import { IPv4 } from '../src/ipv4';

describe('IPv4', () => {
  it('Accepts valid octets', () => {
    new IPv4(0, 0, 0, 0);
    new IPv4(1, 1, 1, 1);
    new IPv4(255, 255, 255, 255);
  });

  it('Rejects invalid octets', () => {
    for (let index = 0; index < 4; ++index) {
      for (let value of [-1, 256]) {
        const octets = [0, 0, 0, 0];
        octets[index] = value;

        expect(() => new IPv4(...octets)).to.throw('Invalid octets for IPv4.');
      }
    }
  });

  it('Converts to number properly', () => {
    for (let value of [
      { octets: [0, 0, 0, 0], expected: 0 },
      { octets: [1, 1, 1, 1], expected: 16843009 },
      { octets: [128, 128, 128, 128], expected: 2155905152 },
      { octets: [255, 255, 255, 255], expected: 4294967295 }
    ]) {
      const ipv4 = new IPv4(...value.octets);
      const number = ipv4.asNumber();
      expect(number).to.equal(value.expected);
    }
  });

  it('Converts to octets properly', () => {
    for (let value of [
      [0, 0, 0, 0],
      [1, 1, 1, 1],
      [128, 128, 128, 128],
      [255, 255, 255, 255]
    ]) {
      const ipv4 = new IPv4(...value);
      const octets = ipv4.asOctets();
      expect(octets).to.have.ordered.members(value);
    }
  });

  it('Converts to string properly', () => {
    for (let value of [
      { octets: [0, 0, 0, 0], expected: '0.0.0.0' },
      { octets: [1, 1, 1, 1], expected: '1.1.1.1' },
      { octets: [128, 128, 128, 128], expected: '128.128.128.128' },
      { octets: [255, 255, 255, 255], expected: '255.255.255.255' }
    ]) {
      const ipv4 = new IPv4(...value.octets);
      const string = ipv4.toString();
      expect(string).to.equal(value.expected);
    }
  });

  it('Accepts valid number for parsing', () => {
    for (let value of [
      { number: 0, octets: [0, 0, 0, 0] },
      { number: 16843009, octets: [1, 1, 1, 1] },
      { number: 2155905152, octets: [128, 128, 128, 128] },
      { number: 4294967295, octets: [255, 255, 255, 255] }
    ]) {
      const ipv4 = IPv4.fromNumber(value.number);
      const octets = ipv4.asOctets();
      expect(octets).to.have.ordered.members(value.octets);
    }
  });

  it('Rejects invalid number for parsing', () => {
    for (let value of [-1, 4294967296]) {
      expect(() => IPv4.fromNumber(value)).to.throw('Invalid number for IPv4.');
    }
  });

  it('Accepts valid string for parsing', () => {
    for (let value of [
      { string: '0.0.0.0', octets: [0, 0, 0, 0] },
      { string: '1.1.1.1', octets: [1, 1, 1, 1] },
      { string: '128.128.128.128', octets: [128, 128, 128, 128] },
      { string: '255.255.255.255', octets: [255, 255, 255, 255] }
    ]) {
      const ipv4 = IPv4.parseString(value.string);
      const octets = ipv4.asOctets();
      expect(octets).to.have.ordered.members(value.octets);
    }
  });

  it('Rejects invalid string for parsing', () => {
    for (let value of ['', 'a.b.c.d', ' 0.0.0.0', '0.0.0.0 ',
      'a.0.0.0', '0.a.0.0', '0.0.a.0', '0.0.0.a',
      '1234.0.0.0', '0.1234.0.0', '1.1.1234.0', '1.1.1.1234',
      '-1.0.0.0', '0.-1.0.0', '0.0.-1.0', '0.0.0.-1',
      '+1.0.0.0', '0.+1.0.0', '0.0.+1.0', '0.0.0.+1'
    ]) {
      expect(() => IPv4.parseString(value)).to.throw('Invalid format for IPv4.')
    }

    for (let value of ['256.0.0.0', '0.256.0.0', '0.0.256.0', '0.0.0.256']) {
      expect(() => IPv4.parseString(value)).to.throw('Invalid octets for IPv4.')
    }
  });
});

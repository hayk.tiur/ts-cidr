/**
 * An IPv4 address.
 */
export class IPv4 {
  /**
   * Octets representing this IPv4 address.
   */
  private readonly Octets: number[]

  /**
   * Create a new IPv4 address.
   *
   * @param octets The four bytes (octets) of the IPv4 address.
   * @throws When octets doesn't have exactly 4 values, a value is not between 0 (inclusive) or 255 (inclusive).
   */
  constructor(...octets: number[]) {
    if (octets == null || octets.length != 4 || octets.some(x => x < 0 || x > 255)) {
      throw new Error('Invalid octets for IPv4.');
    }

    this.Octets = octets;
  }

  /**
   * Converts this IPv4 address to a (32 bit) number.
   *
   * @returns Number representing this IPv4 address.
   */
  public asNumber(): number {
    // Even though we remain within the 32 bits range, when the highest bit of the first octet is set, the bitshift will
    // result in a negative number. This is true, even though number can hold much larger numbers. To overcome this we
    // use BigInt, which won't have this problem.
    const bigIntOctets = this.Octets.map(x => BigInt(x));
    const bigIntResult = bigIntOctets[0] << 24n | bigIntOctets[1] << 16n | bigIntOctets[2] << 8n | bigIntOctets[3];
    return Number(bigIntResult);
  }

  /**
   * Returns the octets of this IPv4 address.
   * This is a copy, so changing the return value doesn't affect this IPv4 address in any way.
   *
   * @returns Octets representing this IPv4 address.
   */
  public asOctets(): number[] {
    return [...this.Octets];
  }

  /**
   * Converts this IPv4 address to its textual representation.
   *
   * @returns String representing this IPv4 address.
   */
  public toString(): string {
    return this.Octets.join('.');
  }

  /**
   * Splits input up into 4 octets and creates a new IPv4 instance.
   * For validation rules, check the constructor.
   *
   * @param input Number representing an IPv4 address.
   * @throws When input is less than 0 or more than 4294967295.
   */
  static fromNumber(input: number): IPv4 {
    if (input < 0 || input > 4294967295) {
      throw new Error('Invalid number for IPv4.');
    }
    return new IPv4(input >> 24 & 255, input >> 16 & 255, input >> 8 & 255, input & 255);
  }

  /**
   * Splits input up into 4 parts by dot. Each part represents an octet with which a new IPv4 instance is created.
   * For additional validation rules, check the constructor.
   *
   * @param input String representing an IPv4 address.
   * @throws When input doesn't represent an IPv4 address, like not containing only digits or 3 dots.
   */
  static parseString(input: string): IPv4 {
    if (input == null || !/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/.test(input)) {
      throw new Error('Invalid format for IPv4.');
    }

    const octets = input.split('.').map(x => parseInt(x));
    return new IPv4(...octets);
  }
}

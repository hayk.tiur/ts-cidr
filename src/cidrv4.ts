import { IPv4 } from './ipv4';

/**
 * An CIDR address based of an IPv4 address.
 */
export class CIDRv4 {
  /**
   * Provided IPv4 address of this CIDR address. It may be any IPv4 address within the range of this CIDR.
   */
  readonly IP: IPv4
  /**
   * Amount of masked (fixed) bits of the IPv4 address.
   */
  readonly Mask: number;

  /**
   * First IPv4 address within the range of this CIDR. May be the same as the last IPv4 address.
   */
  readonly FirstIP: IPv4;
  /**
   * Last IPv4 address within the range of this CIDR. May be the same as the first IPv4 address.
   */
  readonly LastIP: IPv4;
  /**
   * Total amount of IP addresses available within the range of this CIDR.
   */
  readonly TotalIPs: number;

  /**
   * Creates a new CIDR based of an IPv4 address.
   * The first and last IPv4 addresses are automatically calculated, as well as the total amount of IP addresses
   * available within the range of the CIDR.
   *
   * @param ip The IPv4 address in range of the CIDR to create. May be any IPv4 address.
   * @param mask The amount of masked (fixed) bits of the IPv4 address.
   * @throws When ip is null or mask is not between 0 (inclusive) and 32 (inclusive).
   */
  constructor(ip: IPv4, mask: number) {
    if (ip == null) {
      throw new Error('Invalid IPv4 for CIDRv4.');
    }
    if (mask < 0 || mask > 32) {
      throw new Error('Invalid mask for CIDRv4.');
    }

    this.IP = ip;
    this.Mask = mask;

    this.FirstIP = this.getFirstIP();
    this.LastIP = this.getLastIP();
    this.TotalIPs = this.LastIP.asNumber() - this.FirstIP.asNumber() + 1; // First and last IPs are inclusive, thus +1
  }

  /**
   * Splits this CIDR into multiple smaller CIDRs all equal of size. It may be that not all available IP addresses are
   * used.
   *
   * @param count Amount of smaller CIDRs of the same size to split this CIDR into.
   * @returns Set of smaller CIDRs with the size of count
   * @throws When count is less than 2 or larger than the amount of available IP addresses.
   */
  public splitInEqualSubCIDRs(count: number): CIDRv4[] {
    if (count < 2) {
      throw new Error('Must be split in at least 2.');
    }
    if (count > this.TotalIPs) {
      throw new Error('Can\'t split in more than the total IPs available.');
    }

    // Because masks are binary and thus come with the power two, get the amount of bits we have to add to the mask.
    // This power must be equal to or greater than the requested count, else they won't fit.
    const power = Math.ceil(Math.log(count) / Math.log(2));

    // Increment the mask with the power.
    // We know we won't exceed the maximum here, because the amount of total IPs is always a power of two.
    const mask = this.Mask + power;

    // We can also use the power to get the amount of IPs that must be in each sub-CIDR.
    // This makes computing easier as we can just add it to the previous IP.
    const ipsPerCIDR = this.TotalIPs / Math.pow(2, power);

    // Start with the first IP of this CIDR and use the new mask.
    // Then for each successive sub-CIDR, we add the amount of IPs per CIDR until we reached the requested amount.
    const subCIDRs = Array<CIDRv4>(count);
    for (let index = 0, currentIP = this.FirstIP.asNumber(); index < count; ++index, currentIP += ipsPerCIDR) {
      const ip = IPv4.fromNumber(currentIP);
      subCIDRs[index] = new CIDRv4(ip, mask);
    }

    return subCIDRs;
  }

  /**
   * Splits this CIDRv4 into one or more sub CIDRv4. This is done by providing segments, which request the amount of new
   * bits and include a name (so that you can find them back later). The new bits are from the current mask, meaning
   * that when the mask is currently 24 and the amount of new bits is 3, the CIDRv4 will have a mask of 27. This holds
   * true for all segments.
   *
   * There may be gaps in the IPv4 address ranges from the resulting set of CIDRv4. This can be in particular true when
   * different amounts of bits are requested, as we create exactly one CIDRv4 for each segment.
   *
   * @param segments Used for splitting up this CIDRv4 into named sub-CIDRv4.
   * @returns Map with for each segment a CIDRv4 found by the name of the segment.
   * @throws When a segment has less than one bit, no name or no unique name. When more bits are requested than
   *         available or the total of requested bits exceeds the available pool (including considering gaps).
   */
  public splitInSubCIDRs(...segments: { newBits: number, name: string }[]): Map<string, CIDRv4> {
    if (segments.some(x => x.newBits < 1)) {
      throw new Error('All segments must have at least one bit.');
    }
    if (segments.some(x => typeof x.name !== 'string' || x.name.length < 1)) {
      throw new Error('All segments must have a name.');
    }

    const freeBits = 32 - this.Mask;
    if (segments.some(x => x.newBits > freeBits)) {
      throw new Error(`${freeBits} bits available, but a segment with more was given.`);
    }

    // Because not all segments may require the same amount of bits, must keep track how many IPv4 address we've dished
    // out. This is important, because we may have to compensate/skip some due to this difference in bits.
    let availableIPs = BigInt(this.TotalIPs);
    let currentIP = BigInt(this.FirstIP.asNumber());

    // Go through all the segments and sort them from least to most bits. This reduces the amounts of gaps and we start
    // with those that would get the most IPv4 addresses. If too many IPv4 addresses are requested, we'll error out
    // faster this way.
    const result = new Map<string, CIDRv4>();
    for (const segment of segments.sort((x, y) => x.newBits < y.newBits ? -1 : 1)) {
      // The bits of a segment don't stack and are always computed from start of this CIDRv4.
      // This means the amount of IPV4 addresses needed is the power of two from the original free set of bits.
      const neededIPs = 2n ** BigInt(freeBits - segment.newBits);
      if (neededIPs > availableIPs) {
        throw new Error('Too many IPv4 addresses requested.');
      }

      // Because we may have jumped to a different amount of bits, we have to compensate for a gap.
      // If we don't, we would have to return two CIDRv4 to represent the range, which we don't want to do.
      const compensation = availableIPs % neededIPs;
      currentIP += compensation;
      result.set(segment.name, new CIDRv4(IPv4.fromNumber(Number(currentIP)), this.Mask + segment.newBits));

      // We just claimed same IPv4 addresses, we must update our worker variables with that.
      currentIP += neededIPs;
      availableIPs -= neededIPs + compensation;
    }

    // If two or more segments have the same name, we've set a key more than once in the result.
    if (result.size != segments.length) {
      throw new Error('All segments must have an unique name.');
    }

    return result;
  }

  /**
   * Splits this CIDRv4 into one or more sub CIDRv4. The segments are converted to their nearest size in bits (rounded
   * up), meaning a size of 5 would result in 3 bits as 2 bits would only cover 4 IPv4 addresses where 3 covers 8 IPv4
   * addresses. This will result in CIDRv4 too large and that's why it's always recommended to stick with multitudes of
   * 2. Once the mapping is complete, splitInSubCIDRs is invoked and its result returned. See this function for more
   * information about the algorithm and validation rules.
   *
   * @param segments Used for splitting up this CIDRv4 into named sub-CIDRv4.
   */
  public splitInSubCIDRsBySize(...segments: { size: number, name: string }[]) {
    const segmentsMapped = segments.map(x => {
      const closestBits = Math.ceil(Math.log(x.size) / Math.log(2));
      return {
        newBits: 32 - closestBits - this.Mask,
        name: x.name
      };
    });

    return this.splitInSubCIDRs(...segmentsMapped);
  }

  /**
   * Converts this CIDR to its textual representation.
   * Note that the original IPv4 address is used and not the first.
   *
   * @returns String representing this CIDR.
   */
  public toString(): string {
    return `${this.IP.toString()}/${this.Mask}`;
  }

  /**
   * Computes the first IPv4 address within this CIDR.
   *
   * @private
   * @returns First IPv4 address within the range of this CIDR.
   */
  private getFirstIP(): IPv4 {
    // Create a binary textual representation of the mask.
    // This makes it easy to parse it as a number without bit shifting.
    const maskString = `${'1'.repeat(this.Mask)}${'0'.repeat(32 - this.Mask)}`;
    const mask = parseInt(maskString, 2);

    // Combine the octets of the IP into one number.
    // This will make it easier to apply the entire mask at once.
    const ip = this.IP.asNumber();

    // Apply the mask to the ip.
    // The resulting number (32 bits) is the lowest IP possible with the mask within the CIDR.
    const ipMasked = BigInt(ip) & BigInt(mask);

    return IPv4.fromNumber(Number(ipMasked));
  }

  /**
   * Computes the last IPv4 address within the range of this CIDR.
   *
   * @private
   * @returns Last IPv4 address within the range of this CIDR.
   * @throws When the first IPv4 address hasn't been calculated yet.
   */
  private getLastIP(): IPv4 {
    if (this.FirstIP == null) {
      throw new Error('First IPv4 must be calculated first.');
    }

    // All bits not in the mask are the remainder and together are the number of IPv4 addresses in the CIDR.
    // To get to this number easily, create a binary textual representation of the remainder of the mask.
    // Note that add a 0 in front in case there is no remainder; this doesn't effect the result.
    const maskRemainderString = `0${'1'.repeat(32 - this.Mask)}`;
    const maskRemainder = parseInt(maskRemainderString, 2);

    // Combine the octets of the first IP into one number.
    // This will make it easier to add the remainder to.
    const firstIP = this.FirstIP.asNumber();

    // Add the remainder of the mask to the IP, which is the maximum possible within the CIDR.
    // Going 1 higher would mean we'd have to go one mask bit less.
    const lastIP = firstIP + maskRemainder;

    return IPv4.fromNumber(lastIP);
  }

  /**
   * Splits input up into 4 parts by dot and then by a forward slash. The first 4 parts represents an octet with which a
   * new IPv4 instance is created. The last part represents the mask.
   * For additional validation rules, check the constructor.
   *
   * @param input String representing a CIDR with an IPv4 address.
   * @throws When input doesn't represent a CIDR with an IPv4 address.
   */
  static parseString(input: string): CIDRv4 {
    if (input == null || !/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/.test(input)) {
      throw new Error('Invalid format for CIDRv4.');
    }

    const [ipString, maskString] = input.split('/');
    const ip = IPv4.parseString(ipString);
    const mask = parseInt(maskString);
    return new CIDRv4(ip, mask);
  }
}
